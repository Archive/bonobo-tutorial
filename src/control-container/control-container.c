/*
 * control-container.c
 * 
 * Authors:
 *   Martin Baulig (baulig@suse.de)
 *
 * Based on bonobo/samples/control/sample-control-container.c
 *
 * Copyright 2000 Free Software Foundation.
 * Copyright 2000 SuSE GmbH.
 */
#include <config.h>
#include <gnome.h>

#if BONOBO_USES_OAF
#include <liboaf/liboaf.h>
#else
#include <libgnorba/gnorba.h>
#endif

#include <bonobo.h>

static GnomeUIInfo main_toolbar [] = {
    GNOMEUIINFO_END
};

static guint
container_create (gchar *activate_id)
{
    gchar	    *selected_id;
    const gchar	    *required_interfaces [] = {
	"IDL:Bonobo/Control:1.0", NULL
    };

    GtkWidget		*app;
    GtkWidget		*control;
    GtkWidget		*box;
    GtkWidget		*toolbar;
    GnomeAppBar		*appbar;
    BonoboControlFrame	*control_frame;
    BonoboUIHandler	*uih;

    app = gnome_app_new ("control-container",
			 "Bonobo Control Container");
    gtk_window_set_default_size (GTK_WINDOW (app), 500, 440);
    gtk_window_set_policy (GTK_WINDOW (app), TRUE, TRUE, FALSE);

    appbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
    gnome_app_set_statusbar (GNOME_APP (app), GTK_WIDGET (appbar));

    uih = bonobo_ui_handler_new ();

    bonobo_ui_handler_set_app (uih, GNOME_APP (app));
    bonobo_ui_handler_create_menubar (uih);
    bonobo_ui_handler_create_toolbar (uih, "Main");

    bonobo_ui_handler_set_statusbar (uih, GTK_WIDGET (appbar));

    toolbar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
			       GTK_TOOLBAR_BOTH);       
    gnome_app_fill_toolbar (GTK_TOOLBAR (toolbar), main_toolbar, NULL);

    bonobo_ui_handler_set_toolbar (uih, "Test", toolbar);

    box = gtk_vbox_new (FALSE, 0);
    gnome_app_set_contents (GNOME_APP (app), box);

    if (activate_id)
	selected_id = g_strdup (activate_id);
    else
	selected_id = gnome_bonobo_select_id
	    (_("Select which Control to add"), required_interfaces);

    if (selected_id == NULL)
	g_error ("You need to select an object to activate.");

    g_message ("Selected ID `%s'.\n", selected_id);

    control = bonobo_widget_new_control
	(selected_id, bonobo_object_corba_objref (BONOBO_OBJECT (uih)));

    if (control == NULL)
	g_error ("Failed to activate object with ID `%s'.",
		 selected_id);

    control_frame = bonobo_widget_get_control_frame
	(BONOBO_WIDGET (control));

    gtk_box_pack_start (GTK_BOX (box), control, TRUE, TRUE, 0);

    gtk_widget_show_all (app);

    bonobo_control_frame_set_autoactivate (control_frame, FALSE);
    bonobo_control_frame_control_activate (control_frame);

    return FALSE;
}

int
main (int argc, char **argv)
{
    gchar *activate_id = NULL;
    CORBA_Environment ev;
    CORBA_ORB orb;

    CORBA_exception_init (&ev);

#if BONOBO_USES_OAF
    gnome_init_with_popt_table ("control-container", VERSION,
				argc, argv,
				oaf_popt_options, 0, NULL); 
    orb = oaf_init (argc, argv);
#else
    gnome_CORBA_init_with_popt_table
	("control-container", VERSION, &argc, argv, NULL, 0,
	 NULL, GNORBA_INIT_SERVER_FUNC, &ev);

    orb = gnome_CORBA_ORB ();
#endif

    CORBA_exception_free (&ev);

    if (bonobo_init (orb, NULL, NULL) == FALSE)
	g_error ("Could not initialize Bonobo");

    if (argc == 2)
	activate_id = g_strdup (argv [1]);

    /*
     * We can't make any CORBA calls unless we're in the main
     * loop.  So we delay creating the container here.
     */
    gtk_idle_add ((GtkFunction) container_create, activate_id);

    bonobo_main ();

    return 0;
}
