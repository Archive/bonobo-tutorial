/*
 * sample-embeddable.c
 * 
 * Authors:
 *   Martin Baulig (baulig@suse.de)
 *
 * Copyright 2000 Free Software Foundation.
 * Copyright 2000 SuSE GmbH.
 */
#include <config.h>
#include <gnome.h>

#if BONOBO_USES_OAF
#include <liboaf/liboaf.h>
#else
#include <libgnorba/gnorba.h>
#endif

#include <bonobo.h>
#include <glade/glade.h>

#if BONOBO_USES_OAF
#define FACTORY_IID "OAFIID:bonobo-tutorial-embeddable-factory:sample:71359218-c8f9-4838-adb7-128ee6adc8f6"
#else
#define FACTORY_IID "embeddable-factory:sample"
#endif

static void
do_verb (BonoboView *view, const char *verb_name, void *closure)
{
    g_message ("do_verb (\"%s\")", verb_name);
}

static GtkWidget *
create_widget (const char *rootnode)
{
    GladeXML *xml;
    GtkWidget *widget;
    const char *filename;

    filename = BONOBOTUTORIALDIR "/sample-embeddable.glade";
    xml = glade_xml_new (filename, rootnode);
    if (!xml) {
	g_warning ("Failed to read glade file `%s'", filename);
	return NULL;
    }

    widget = glade_xml_get_widget (xml, rootnode);
    if (!widget) {
	g_warning ("Failed to get root node `%s' from glade file '%s'",
		   rootnode, filename);
	return NULL;
    }

    glade_xml_signal_autoconnect (xml);

    return widget;
}

static BonoboView *
sample_view_factory (BonoboEmbeddable *embeddable,
		     const Bonobo_ViewFrame view_frame,
		     void *closure)
{
    BonoboView *view;
    GtkWidget *widget;

    widget = create_widget ((const char *) closure);
    gtk_widget_show (widget);

    view = bonobo_view_new (widget);

    bonobo_view_register_verb (view, "test", do_verb, closure);

    return view;
}

static BonoboObject *
sample_embeddable_factory (BonoboGenericFactory *Factory, void *closure)
{
    BonoboEmbeddable *embeddable;

    embeddable = bonobo_embeddable_new (sample_view_factory, closure);

    return BONOBO_OBJECT (embeddable);
}

static void
initialize_factory (void)
{
    static BonoboGenericFactory *embeddable_factory = NULL;

    embeddable_factory = bonobo_generic_factory_new
	(FACTORY_IID, sample_embeddable_factory, "sample-embeddable");
}

int
main (int argc, char **argv)
{
    CORBA_Environment ev;
    CORBA_ORB orb;

    CORBA_exception_init (&ev);

#if BONOBO_USES_OAF
    gnome_init_with_popt_table ("sample-embeddable", VERSION,
				argc, argv,
				oaf_popt_options, 0, NULL); 
    orb = oaf_init (argc, argv);
#else
    gnome_CORBA_init_with_popt_table
	("sample-embeddable", VERSION, &argc, argv, NULL, 0,
	 NULL, GNORBA_INIT_SERVER_FUNC, &ev);

    orb = gnome_CORBA_ORB ();
#endif

    CORBA_exception_free (&ev);

    glade_gnome_init ();

    if (bonobo_init (orb, NULL, NULL) == FALSE)
	g_error ("Could not initialize Bonobo");

    initialize_factory ();

    bonobo_main ();

    return 0;
}
