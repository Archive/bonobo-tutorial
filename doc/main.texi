@node Top, Components, (dir), (dir)

@menu
* Components::                  Writing Components.


@detailmenu --- The Detailed Node Listing ---

Writing Components

* Existing Containers::         Existing Container Applications.
* GOAD and OAF::                Making your code work with GOAD and OAF.
* Initialization::              Program initialization.
* Factory Objects::             The @code{Bonobo::GenericFactory} object.
* BonoboUIInfo::                Managing UI elements (menus, toolbars).
* Bonobo Control::              Bonobo Controls.
* Bonobo Embeddable::           Embeddable components.
* Storage Modules::             Storage Modules.

GOAD and OAF

* GOAD-ID and OAFIID::          Identifying components.

The @code{Bonobo::GenericFactory} object

* Creating a factory object::   @code{bonobo_generic_factory_new}
* Multiple factory objects::    @code{bonobo_generic_factory_new_multi}

The BonoboUIInfo Object

* BonoboUIInfo Toolbars::       Adding toolbars to your component.
* BonoboUIInfo Statusbar::      Displaying menu hints in the statusbar.

Bonobo Controls

* Creating a BonoboControl::    Creating a new Bonobo Control.
* The ControlFrame Object::     The Servant's handle to the Control.
* Control Menus::               Providing Menus.
* Control Toolbars::            Providing Toolbars.

Embeddable components

* Creating a BonoboEmbeddable::  How to create a @code{BonoboEmbeddable}.
* Creating a BonoboView::       How to create a @code{BonoboView}.
* Verbs::                       Simple actions of a @code{BonoboView}.

Storage Modules

* BonoboPersistStream::         Data stream.
* BonoboPersistFile::           Local file.
* BonoboProgressiveDataSink::   Progressive loading/saving.

@end detailmenu
@end menu

@include components.texi
