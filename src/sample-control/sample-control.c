/*
 * control-container.c
 * 
 * Authors:
 *   Martin Baulig (baulig@suse.de)
 *
 * Copyright 2000 Free Software Foundation.
 * Copyright 2000 SuSE GmbH.
 */
#include <config.h>
#include <gnome.h>

#if BONOBO_USES_OAF
#include <liboaf/liboaf.h>
#else
#include <libgnorba/gnorba.h>
#endif

#include <bonobo.h>
#include <glade/glade.h>

#if BONOBO_USES_OAF
#define FACTORY_IID "OAFIID:bonobo-tutorial-control-factory:sample:eacb7cc0-5723-4267-9dc3-b8fcc4be56e4"
#else
#define FACTORY_IID "control-factory:sample"
#endif

GnomeUIInfo test_menu [] = {
    GNOMEUIINFO_MENU_NEW_ITEM (N_("_Test"),
			       N_("Test menu item"),
			       NULL, NULL),
    GNOMEUIINFO_END
};

static GnomeUIInfo sample_component_menu [] = {
    GNOMEUIINFO_SUBTREE (N_("Test"), test_menu),
    GNOMEUIINFO_END
};

static GnomeUIInfo sample_component_toolbar [] = {
    GNOMEUIINFO_ITEM_STOCK (N_("Test"), NULL, NULL,
			    GNOME_STOCK_PIXMAP_REFRESH),
    GNOMEUIINFO_END
};

static GtkWidget *
create_widget (const char *rootnode)
{
    GladeXML *xml;
    GtkWidget *widget;
    const char *filename;

    filename = BONOBOTUTORIALDIR "/sample-control.glade";
    xml = glade_xml_new (filename, rootnode);
    if (!xml) {
	g_warning ("Failed to read glade file `%s'", filename);
	return NULL;
    }

    widget = glade_xml_get_widget (xml, rootnode);
    if (!widget) {
	g_warning ("Failed to get root node `%s' from glade file '%s'",
		   rootnode, filename);
	return NULL;
    }

    glade_xml_signal_autoconnect (xml);

    return widget;
}

static void
activate_cb (BonoboControl *control, gboolean state, void *closure)
{
    BonoboUIHandler *local_ui_handler;
    BonoboUIHandlerToolbarItem *toolbar_list;

    fprintf (stderr, "activate - %p - %d\n", control, state);

    local_ui_handler = bonobo_control_get_ui_handler
	(BONOBO_CONTROL (control));

    toolbar_list = bonobo_ui_handler_toolbar_parse_uiinfo_list
	(sample_component_toolbar);
    bonobo_ui_handler_toolbar_add_list (local_ui_handler, "/Main",
					toolbar_list);
    bonobo_ui_handler_toolbar_free_list (toolbar_list);
}

static BonoboObject *
sample_control_factory (BonoboGenericFactory *Factory, void *closure)
{
    BonoboControl *control;
    GtkWidget	  *widget;

    /* Create the control. */
    widget = create_widget ((const char *) closure);
    gtk_widget_show (widget);

    control = bonobo_control_new (widget);

    bonobo_control_set_automerge (control, TRUE);
    bonobo_control_set_menus (control, sample_component_menu);

    gtk_signal_connect (GTK_OBJECT (control), "activate",
			GTK_SIGNAL_FUNC (activate_cb), closure);

    return BONOBO_OBJECT (control);
}

static void
initialize_factory (void)
{
    static BonoboGenericFactory *control_factory = NULL;

    control_factory = bonobo_generic_factory_new
	(FACTORY_IID, sample_control_factory, "sample-control");
}

int
main (int argc, char **argv)
{
    CORBA_Environment ev;
    CORBA_ORB orb;

    CORBA_exception_init (&ev);

#if BONOBO_USES_OAF
    gnome_init_with_popt_table ("control-container", VERSION,
				argc, argv,
				oaf_popt_options, 0, NULL); 
    orb = oaf_init (argc, argv);
#else
    gnome_CORBA_init_with_popt_table
	("control-container", VERSION, &argc, argv, NULL, 0,
	 NULL, GNORBA_INIT_SERVER_FUNC, &ev);

    orb = gnome_CORBA_ORB ();
#endif

    CORBA_exception_free (&ev);

    glade_gnome_init ();

    if (bonobo_init (orb, NULL, NULL) == FALSE)
	g_error ("Could not initialize Bonobo");

    initialize_factory ();

    bonobo_main ();

    return 0;
}
