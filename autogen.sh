#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Bonobo Tutorial"

(test -f $srcdir/configure.in \
  && test -d $srcdir/doc \
  && test -f $srcdir/doc/bonobo-tutorial.texi) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

. `gnome-config --bindir`/gnome-autogen.sh

