/*
 * embeddable-container.c
 * 
 * Authors:
 *   Martin Baulig (baulig@suse.de)
 *
 * Copyright 2000 Free Software Foundation.
 * Copyright 2000 SuSE GmbH.
 */
#include <config.h>
#include <gnome.h>

#if BONOBO_USES_OAF
#include <liboaf/liboaf.h>
#else
#include <libgnorba/gnorba.h>
#endif

#include <bonobo.h>

static void
test_cb (GtkWidget *widget, void *closure)
{
    g_return_if_fail (closure != NULL);
    g_return_if_fail (BONOBO_IS_VIEW_FRAME (closure));

    bonobo_view_frame_view_do_verb (BONOBO_VIEW_FRAME (closure),
				    "test");
}

GnomeUIInfo test_menu [] = {
    GNOMEUIINFO_MENU_NEW_ITEM (N_("_Test"),
			       N_("Test menu item"),
			       test_cb, NULL),
    GNOMEUIINFO_END
};

static GnomeUIInfo main_menu [] = {
    GNOMEUIINFO_SUBTREE (N_("Test"), test_menu),
    GNOMEUIINFO_END
};

static void
create_menus (BonoboUIHandler *uih, void *closure)
{
    BonoboUIHandlerMenuItem *menu_list;

    menu_list = bonobo_ui_handler_menu_parse_uiinfo_list_with_data
	(main_menu, closure);
    bonobo_ui_handler_menu_add_list (uih, "/", menu_list);
    bonobo_ui_handler_menu_free_list (menu_list);
}

static guint
container_create (gchar *activate_id)
{
    gchar	    *selected_id;
    const gchar	    *required_interfaces [] = {
	"IDL:Bonobo/Embeddable:1.0", NULL
    };

    GtkWidget		*app;
    GtkWidget		*box;
    GtkWidget		*appbar;
    BonoboContainer	*container;
    BonoboClientSite	*client_site;
    BonoboObjectClient	*object_client;
    BonoboViewFrame	*view_frame;
    GtkWidget		*view_widget;
    BonoboUIHandler	*uih;

    app = gnome_app_new ("embeddable-container",
			 "Bonobo Embeddable Container");
    gtk_window_set_default_size (GTK_WINDOW (app), 500, 440);
    gtk_window_set_policy (GTK_WINDOW (app), TRUE, TRUE, FALSE);

    appbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
    gnome_app_set_statusbar (GNOME_APP (app), GTK_WIDGET (appbar));

    uih = bonobo_ui_handler_new ();

    bonobo_ui_handler_set_app (uih, GNOME_APP (app));
    bonobo_ui_handler_create_menubar (uih);
    bonobo_ui_handler_create_toolbar (uih, "Main");

    bonobo_ui_handler_set_statusbar (uih, GTK_WIDGET (appbar));

    box = gtk_vbox_new (FALSE, 0);
    gnome_app_set_contents (GNOME_APP (app), box);

    container = bonobo_container_new ();
    client_site = bonobo_client_site_new (container);

    if (activate_id)
	selected_id = g_strdup (activate_id);
    else
	selected_id = gnome_bonobo_select_id
	    (_("Select which Embeddable to add"), required_interfaces);

    if (selected_id == NULL)
	g_error ("You need to select an object to activate.");

    g_message ("Selected ID `%s'.\n", selected_id);

    object_client = bonobo_object_activate (selected_id, 0);

    if (object_client == NULL)
	g_error ("Failed to activate object with ID `%s'.",
		 selected_id);

    bonobo_client_site_bind_embeddable (client_site, object_client);

    view_frame = bonobo_client_site_new_view_full
	(client_site, bonobo_object_corba_objref (BONOBO_OBJECT (uih)),
	 FALSE, TRUE);

    if (!view_frame)
	g_error ("Create view failed.");

    create_menus (uih, view_frame);

    view_widget = bonobo_view_frame_get_wrapper (view_frame);

    gtk_box_pack_start (GTK_BOX (box), view_widget, TRUE, TRUE, 0);

    gtk_widget_show_all (app);

    return FALSE;
}

int
main (int argc, char **argv)
{
    gchar *activate_id = NULL;
    CORBA_Environment ev;
    CORBA_ORB orb;

    CORBA_exception_init (&ev);

#if BONOBO_USES_OAF
    gnome_init_with_popt_table ("embeddable-container", VERSION,
				argc, argv,
				oaf_popt_options, 0, NULL); 
    orb = oaf_init (argc, argv);
#else
    gnome_CORBA_init_with_popt_table
	("embeddable-container", VERSION, &argc, argv, NULL, 0,
	 NULL, GNORBA_INIT_SERVER_FUNC, &ev);

    orb = gnome_CORBA_ORB ();
#endif

    CORBA_exception_free (&ev);

    if (bonobo_init (orb, NULL, NULL) == FALSE)
	g_error ("Could not initialize Bonobo");

    if (argc == 2)
	activate_id = g_strdup (argv [1]);

    /*
     * We can't make any CORBA calls unless we're in the main
     * loop.  So we delay creating the container here.
     */
    gtk_idle_add ((GtkFunction) container_create, activate_id);

    bonobo_main ();

    return 0;
}
